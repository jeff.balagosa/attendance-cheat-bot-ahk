﻿; IMPORTANT! PLEASE CONFIGURE AND TEST THIS SCRIPT BEFORE USING IT ON THE REAL PAGE!
; The mock page you can use to configure and test this script is located in the resources folder of this repo.
; Refer to the README and all comments in this file for more info.

#Requires AutoHotkey v2.0

; These params in the function call need to be updated to accomodate your own monitor's resolution and internet speed.
; AHK has a nice built-in script called "Windows Spy," which you can use to find the x and y coordinates of the button you want to click.
; For your convenience, I've included a shortcut of Windows Spy in the resources folder of this repo. It should work if you installed AHK in the default location.
; You should be able to find it in the docs: https://www.autohotkey.com/docs/v2/
F5::SubmitForm(493, 294, 500)

; Use escape key to exit the script
Esc::ExitApp

SubmitForm(x, y, sleepTime) {
  SetTitleMatchMode 2
  WinActivate "Galvanize SIS : SJP Aug 2023 PT - Google Chrome"

  while (true) {
    Send "^r"
    Sleep sleepTime
    Click x, y, 2
    Send "^c"
    Send "{Tab}"
    Send "^v" 
    Send "{Tab}"   
    Send "{Enter}"
    Sleep sleepTime
  }    
}
