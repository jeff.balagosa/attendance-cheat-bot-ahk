
# Attendance-Cheater-Bot

## Description:

This script is designed to automatically submit students' Hack Reactor attendance tokens. Every cohort at Hack Reactor has a non-stakes competition for submitting attendance tokens. This bot makes the process (almost) hands-free and (almost) guarantees you a first-place win if you use it. Use responsibly, and may God have mercy on your competitors' souls!

## Features:

- **Automated Submission**: The bot refreshes the page and submits the token, allowing you to focus on the joy of watching your classmates try to win in vain.
- **Customizable**: Coordinates for submission can be customized based on your monitor's resolution.
- **Windows Compatible**: Unfortunately, this script is only compatible with Windows. Apologies to MacOS users! (I am working on a Python version that will be cross-platform compatible. Stay tuned!)

## How to Use:

1. **Requirements**: This script requires [AutoHotkey v2](https://www.autohotkey.com/v2/). Make sure you have it installed.
2. **Configuration**: Update the `x`, `y`, and `sleepTime` parameters in the `SubmitForm(x, y, sleepTime)` function call to match your monitor's resolution and the speed your browser refreshes the page. The `sleepTime` parameter is in milliseconds. You can use the _WindowsSpy_ shortcut in the resources folder to help you find the coordinates. It should work if you installed AHK v2 in the default location.
3. **Running the Script**: 
    - Open the token page.
    - Double-click on the script to run it.
    - Ensure the token page is the active window.
    - Start the process by pressing `F5` on your keyboard.
4. **Exiting the Script**: Use `esc` key to exit the script at any time.

## Testing:

- I've included a _resources_ directory in this repo with a test page for your convenience. You can use this to test the script and ensure it works properly. 
    - If the "I'm here!" button is clicked, several page items will change. Don't worry, it's pretty obvious when it works.
    - Be sure to refresh the page back to normal between tests.

## Troubleshooting:

- Sometimes you have to spam the `esc` key to exit the script. It's usually because the initial press happened during a process of the infinite loop. (I've never had to press it more than 3 times.)
- The functions in the script are pretty self-explanatory. If you're having trouble, try reading the code. AHK is a pretty human readable language. I also have a few comments in there to help you out.
- If all else fails, read the [Documentation](https://www.autohotkey.com/docs/v2/)! May as well get used to it now. It's a big part of being a professional developer.

## Technologies:

- [AutoHotkey v2](https://www.autohotkey.com/v2/) 
    - Documentation can be found [here:](https://www.autohotkey.com/docs/v2/)

## Disclaimer:

**Use this bot at your own risk!** I'm not liable if you break your machine (Not sure how that's even possible, to be honest. But haters gonna hate!) or get dismissed from the program while using this script. Ensure you follow all the guidelines and rules Hack Reactor and your cohort set. I will **not** offer any support for this script; you are all competent programmers and can figure it out independently.

## License:

MIT License

Copyright (c) 2023 Jeff Balagosa

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software") to deal
in the software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the software, and to permit persons to whom the software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the software.

THE SOFTWARE IS PROVIDED "AS IS"WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
